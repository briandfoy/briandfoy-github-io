---
layout: post
title: Clumpers and Splitters
categories:
tags:
stopwords:
last_modified:
original_url:
---

There are two types of people in the world.

I ran across the terms "clumpers" and "splitters" while reading about
the trade-offs in monolithic software thingys (say, a function) and
strictly decomposed version of that.

blah blah blah

# Gorillas in the library

This same thing happens in other domains. In information science,
something like the Dewey Decimal Classification either clumps or
splits. But, the clumps or splits it makes have consequences (the
"ghetto" or the "diaspora").

For example, where does something like Dian Fossey's [Gorillas in the
Mist](https://amzn.to/3qWNrsU) go? It lands in 599, which is the
subcategory "Mammals". But, it's literally "Social Science", which is
the 300 area in the Dewey system. But 300 isn't actually "Social
Science". That category might be better called "Human Society".

And this is exactly why people think the Dewey system is racist; any
classification has to choose some system to operate within. Dewey's is
that from a white American man who lived in the 19th century. We can't
be too hard on him though, because people will say the same thing
about your system a hundred and fifty years from now.





