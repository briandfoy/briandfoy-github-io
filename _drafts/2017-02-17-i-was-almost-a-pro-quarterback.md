---
layout: post
title: I was almost a Pro Quarterback
categories:
tags:
stopwords:
last_modified:
original_url:
---

I'm trying to make a statistics joke about how I was almost a pro quarterback.

So, the requirements, from general to specific

* Be Alive
* Be male
* Be in the America (no one plays it outside North America)
* Go to college
* Graduate
* Be selected by a pro team

So, if you look at all the people in world and filter to through this
sieve, I think I was probably statistically really close. Now I just
need the numbers.

So, in rough numbers, I'm already in the top 5% by step three. You're
probably slightly ahead of me merely because you like football.

* 7,000,000,000 (World Bank: 7,125,000,000)
* 3,500,000,000 of 7,000,000,000 (top 50%) (CIA Fact Book: 101:100)
* 350,000,000 of 7,000,000,000 (top 5%) (US Census Bureau)
* ... need demo data for age groups
* ... (65.9 percent of high school grads go to college: NY Times)
* ... number selected by NFL (get from NFL Players Association)

